<?php
/**
 * @file
 * Gojs theme
 *
 * Render the Map
 */


?>
<div id="sample">
  <div id="myDiagram" style="background-color: whitesmoke; border: solid 1px black; width: <?php echo $width;  ?>; height: <?php echo $height;  ?>"></div>
</div>

<script id="code">
 
    var $ = go.GraphObject.make;  // for conciseness in defining templates

    myDiagram =
      $(go.Diagram, "myDiagram",  // must name or refer to the DIV HTML element
        {
          // start everything in the middle of the viewport
          initialContentAlignment: go.Spot.Left,
          // have mouse wheel events zoom in and out instead of scroll up and down
          "toolManager.mouseWheelBehavior": go.ToolManager.WheelZoom,
          // support double-click in background creating a new node
         
          // enable undo & redo
          "undoManager.isEnabled": true
        });

    

    // define the Node template
    myDiagram.nodeTemplate =
      $(go.Node, "Auto",
        new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify),
        // define the node's outer shape, which will surround the TextBlock
        $(go.Shape, "Rectangle",
          { fill: $(go.Brush, go.Brush.Linear, { 0: "rgb(254, 201, 0)", 1: "rgb(254, 162, 0)" }),
            stroke: "black" }),
        $(go.TextBlock,
          { font: "bold 10pt helvetica, bold arial, sans-serif",
            margin: 4 },
          new go.Binding("text", "text").makeTwoWay()),
		  {
        toolTip:  // define a tooltip for each node that displays the color as text
          $(go.Adornment, "Auto",
            $(go.Shape, { fill: "#FFFFCC" }),
            $(go.TextBlock, { margin: 4 },
              new go.Binding("text", "tooltip"))
          )  // end of Adornment
      }
      );
	myDiagram.groupTemplate =
    $(go.Group, "Vertical",
      $(go.Panel, "Auto",
        $(go.Shape, "RoundedRectangle",  // surrounds the Placeholder
          { parameter1: 14,
            fill: "rgba(128,128,128,0.33)" }),
        $(go.Placeholder,    // represents the area of all member parts,
          { padding: 5})  // with some extra padding around them
      ),
      $(go.TextBlock,         // group title
        { alignment: go.Spot.Right, font: "Bold 12pt Sans-Serif" },
        new go.Binding("text", "key"))
    );

    // unlike the normal selection Adornment, this one includes a Button
    myDiagram.nodeTemplate.selectionAdornmentTemplate =
      $(go.Adornment, "Spot",
        $(go.Panel, "Auto",
          $(go.Shape, { fill: null, stroke: "blue", strokeWidth: 2 }),
          $(go.Placeholder)  // this represents the selected Node
        )
      ); // end Adornment

    // clicking the button inserts a new node to the right of the selected node,
    // and adds a link to that new node
    function addNodeAndLink(e, obj) {
      var adorn = obj.part;
      e.handled = true;
      var diagram = adorn.diagram;
      diagram.startTransaction("Add State");

      // get the node data for which the user clicked the button
      var fromNode = adorn.adornedPart;
      var fromData = fromNode.data;
      // create a new "State" data object, positioned off to the right of the adorned Node
      var toData = { text: "new" };
      var p = fromNode.location.copy();
      p.x += 200;
      toData.loc = go.Point.stringify(p);  // the "loc" property is a string, not a Point object
      // add the new node data to the model
      var model = diagram.model;
      model.addNodeData(toData);
      
      // create a link data from the old node data to the new node data
      var linkdata = {
        from: model.getKeyForNodeData(fromData),  // or just: fromData.id
        to: model.getKeyForNodeData(toData),
        text: "transition"
      };
      // and add the link data to the model
      model.addLinkData(linkdata);
      
      // select the new Node
      var newnode = diagram.findNodeForData(toData);
      diagram.select(newnode);
      
      diagram.commitTransaction("Add State");
      
      // if the new node is off-screen, scroll the diagram to show the new node
      diagram.scrollToRect(newnode.actualBounds);
    }

    // replace the default Link template in the linkTemplateMap
    myDiagram.linkTemplate =
      $(go.Link,  // the whole link panel
        { curve: go.Link.Bezier, adjusting: go.Link.Stretch, reshapable: true },
        new go.Binding("points").makeTwoWay(),
        new go.Binding("curviness", "curviness"),
        $(go.Shape,  // the link shape
          { isPanelMain: true, stroke: "black", strokeWidth: 1.5 }),
        $(go.Shape,  // the arrowhead
          { toArrow: "standard", stroke: null }),
        $(go.Panel, "Auto",
          $(go.Shape,  // the link shape
            {
              fill: $(go.Brush, go.Brush.Radial,
                      { 0: "rgb(240, 240, 240)", 0.3: "rgb(240, 240, 240)", 1: "rgba(240, 240, 240, 0)" }),
              stroke: null
            }),
          $(go.TextBlock, "transition",  // the label
            {
              textAlign: "center",
              font: "10pt helvetica, arial, sans-serif",
              stroke: "black",
              margin: 4,
              editable: true  // editing the text automatically updates the model data
            },
            new go.Binding("text", "text").makeTwoWay())
        )
      );
	  <?php
	if($movable_in_browser){  
	  ?>
myDiagram.isReadOnly = false;  // read-only to avoid accidentally moving any Part in document coordinates
<?php

}else{
?>
myDiagram.isReadOnly = true;

<?php
}
?>
    // create the model for the concept map
    <?php
	if(sizeof($gojs) > 0){
	
	?>
	var nodeDataArray = [
      <?php
	  while(list($key,$value) = each($gojs)){
	  $string = "";
	  if(isset($value["is_group"])) {
		$string = ",isGroup:true";
	  }
	  if(isset($value["group"])) {
		$group = $value["group"];
		$string .= ',group:"' . $group . '"';
	  }
	  
	  ?>
	  
	 
	  { key: "<?php echo $key;  ?>",  text: "<?php echo $key;  ?>" <?php  echo $string; ?> },
	  
	  
	  <?php
	  }
	  ?>
    ];
    var linkDataArray = [
      
      <?php
	  reset($gojs);
	  while(list($key,$value) = each($gojs)){
	  ?>
	  <?php
	  if(isset($value['destination']) && is_array($value['destination'])) {
	  while(list($des_key,$destination) = each($value['destination'])){
	  ?>
	  { from: "<?php echo $key;  ?>", to: "<?php echo $destination['destination'];  ?>" },
      
	  
	  <?php
	  }
	  }
	  ?>
	  
	  <?php
	  }
	  ?>
    ];
    myDiagram.model = new go.GraphLinksModel(nodeDataArray, linkDataArray);
	<?php
	}
	?>
  
</script>