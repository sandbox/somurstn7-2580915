<?php
/**
 * @file
 * Preprocess functions for gojs.
 */

/**
 * Builds the gojs structure.
 */
function gojs_preprocess_views_view_gojs(&$variables) {
 	$path = drupal_get_path('module', 'gojs');
	drupal_add_js($path . '/js/go.js');
	$view = & $variables['view'];
	$go_js["Chassis1"]['is_group'] 	= true;
	
	$go_js["Chassis2"]['is_group'] 	= true;
	$go_js["Plugin1"]['is_group'] 	= true;
	$go_js["Plugin1"]['group'] 	= "Chassis1";
	$go_js["Plugin2"]['is_group'] 	= true;
	$go_js["Plugin2"]['group'] 	= "Chassis2";
	$go_js["Plugin3"]['is_group'] 	= true;
	$go_js["Plugin3"]['group'] 	= "Chassis2";
	$go_js["Plugin4"]['is_group'] 	= true;
	$go_js["Plugin4"]['group'] 	= "Chassis2";
	$go_js["Interface1"]['group'] 	= "Plugin1";
	$go_js["Interface2"]['group'] 	= "Plugin1";
	$go_js["Interface3"]['group'] 	= "Plugin1";
	$go_js["Interface4"]['group'] 	= "Plugin2";
	$go_js["Interface5"]['group'] 	= "Plugin3";
	$go_js["Interface6"]['group'] 	= "Plugin4";
	$go_js["Interface1"]['destination'][] = array('destination' => "Interface4");
	$go_js["Interface2"]['destination'][] = array('destination' => "Interface5");
	$go_js["Interface3"]['destination'][] = array('destination' => "Interface6");
	
	$variables['gojs'] = $go_js;
	$variables['width'] = $view->style_options['width'];
	$variables['height'] = $view->style_options['height'];
	$variables['movable_in_browser'] = $view->style_options['movable_in_browser'];
}


function _get_value_from_array($array){
	$value_to_send = "";
	if(!is_null($array) && is_array($array)) {
		while(list($key,$value) = each($array)){
		
			if(!is_null($array) && is_array($value)){
			
				$value_to_send = _get_value_from_array($value);
			
			}else{
			
				$value_to_send = $value;
			}
		
		}
	}
	return $value_to_send;
}