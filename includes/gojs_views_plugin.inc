<?php

/**
 * @file
 * Definition of the view
 */

/**
 * Extending the view_plugin_style class to provide a gojs style.
 */
class gojs_views_plugin extends views_plugin_style {

  /**
   * Init.
   */
  function init(&$view, &$display, $options = NULL) {
    parent::init($view, $display, $options);

    if (empty($options)) {
      return;
    }

    if (isset($options['pager']) && $options['pager']) {
     
      $this->display->handler->default_display->options['pager']['options']['items_per_page'] = 0;
    }
  }

  /**
   * Set default options.
   */
  function option_definition() {
    $options = parent::option_definition();
	$options['source'] = array('default' => '');
	$options['source_title'] = array('default' => '');
	$options['destination'] = array('default' => '');
	$options['destination_title'] = array('default' => '');
	$options['source_group'] = array('default' => '');
	$options['destination_group'] = array('default' => '');
	
	$options['source_x_axis'] = array('default' => 0);
    $options['source_y_axis'] = array('default' => 0);
	
	$options['destination_x_axis'] = array('default' => 0);
    $options['destination_y_axis'] = array('default' => 0);
	
	$options['link_label'] = array('default' => '');
    $options['height'] = array('default' => '960px');
    $options['width'] = array('default' => '480px');
	$options['movable_in_browser'] = array('default' => 1);
	
	$options['source_tooltip'] = array('default' => '');
	$options['destination_tooltip'] = array('default' => '');

    return $options;
  }

  /**
   * Normalize a list of columns based upon the fields that are
   * available. This compares the fields stored in the style handler
   * to the list of fields actually in the view, removing fields that
   * have been removed and adding new fields in their own column.
   *
   * - Each field must be in a column.
   * - Each column must be based upon a field, and that field
   * is somewhere in the column.
   * - Any fields not currently represented must be added.
   * - Columns must be re-ordered to match the fields.
   *
   * @param $columns
   * An array of all fields; the key is the id of the field and the
   * value is the id of the column the field should be in.
   * @param $fields
   * The fields to use for the columns. If not provided, they will
   * be requested from the current display. The running render should
   * send the fields through, as they may be different than what the
   * display has listed due to access control or other changes.
   */
  function sanitize_columns($columns, $fields = NULL) {
    $sanitized = array();
    if ($fields === NULL) {
      $fields = $this->display->handler->get_option('fields');
    }
    // Preconfigure the sanitized array so that the order is retained.
    foreach ($fields as $field => $info) {
      // Set to itself so that if it isn't touched, it gets column
      // status automatically.
      $sanitized[$field] = $field;
    }
    if (is_array($columns)) {
      foreach ($columns as $field => $column) {
        // Make sure the field still exists.
        if (isset($sanitized[$field])) {
          $sanitized[$field] = $column;
        }
      }
    }
    return $sanitized;
  }

  /**
   * Add settings for the particular jqgrid.
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $handlers = $this->display->handler->get_handlers('field');
    if (!isset($this->options['columns'])) {
      $this->options['columns'] = array();
    }

    $columns = $this->sanitize_columns($this->options['columns']);
    $field_labels = $this->display->handler->get_field_labels();
    if (empty($columns)) {
      $form['error_markup'] = array(
        '#value' => t('You need at least one field before you can configure your gojs settings'),
        '#prefix' => '<div class="error form-item description">',
        '#suffix' => '</div>',
      );
      return;
    }
	
	
	$form['source_group'] = array(
		'#title' => 'Source Group',
        '#type' => 'select',
        '#options' => $columns,
        '#default_value' => $this->options['source_group'],
          
    );
	
	$form['destination_group'] = array(
		'#title' => 'Destination Group',
        '#type' => 'select',
        '#options' => $columns,
        '#default_value' => $this->options['destination_group'],
          
    );
	$form['source'] = array(
		'#title' => 'Source ID',
        '#type' => 'select',
        '#options' => $columns,
        '#default_value' => $this->options['source'],
          
    );
	$form['source_title'] = array(
		'#title' => 'Source Label',
        '#type' => 'select',
        '#options' => $columns,
        '#default_value' => $this->options['source_title'],
          
    );
	
	
	$form['source_tooltip'] = array(
      '#type' => 'textarea',
      '#title' => t('Source tooltip'),
      '#description' => t('Please use Replacement Patterns for tooltip'),
      '#default_value' => $this->options['source_tooltip'],
     
    );
	    
    
	
	$form['destination'] = array(
		'#title' => 'Destination ID',
        '#type' => 'select',
        '#options' => $columns,
        '#default_value' => $this->options['destination'],
          
    );
	
	$form['destination_title'] = array(
		'#title' => 'Destination Label',
        '#type' => 'select',
        '#options' => $columns,
        '#default_value' => $this->options['destination_title'],
          
    );
	$form['destination_tooltip'] = array(
      '#type' => 'textarea',
      '#title' => t('Destination Tooltip'),
      '#description' => t('Please use Replacement Patterns for tooltip'),
      '#default_value' => $this->options['destination_tooltip'],
     
    );
	
	foreach ($this->view->display_handler->get_handlers('field') as $field => $handler) {
      $options[t('Fields')]["[$field]"] = $handler->ui_name();
            
    }
    $count = 0; // This lets us prepare the key as we want it printed.
    foreach ($this->view->display_handler->get_handlers('argument') as $arg => $handler) {
      $options[t('Arguments')]['%' . ++$count] = t('@argument title', array('@argument' => $handler->ui_name()));
      $options[t('Arguments')]['!' . $count] = t('@argument input', array('@argument' => $handler->ui_name()));
    }
    if (!empty($options)) {
      $patterns = t('<p>The following tokens are available for this view. </p>');
      foreach (array_keys($options) as $type) {
        if (!empty($options[$type])) {
          $items = array();
          foreach ($options[$type] as $key => $value) {
            $items[] = $key . ' == ' . $value;
          }
          $patterns .= theme('item_list',
            array(
              'items' => $items,
              'type' => $type
            ));
        }
      }
    }
	
	$form['link_label'] = array(
		  '#title' => 'Link Label',  		
          '#type' => 'textarea',
          '#description' => t('Please use Replacement Patterns for Link Label'),
          '#default_value' => $this->options['link_label'],
          
    );
	$form['patterns'] = array(
      '#type' => 'fieldset',
      '#title' => t('Replacement patterns'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#value' => $patterns,
    );
	
	
	
	$form['source_x_axis'] = array(
		  '#title' => 'Source X-Axis',  		
          '#type' => 'select',
          '#options' => $columns,
          '#default_value' => $this->options['source_x_axis'],
          
    );
	$form['source_y_axis'] = array(
		  '#title' => 'Source Y-Axis',  		
          '#type' => 'select',
          '#options' => $columns,
          '#default_value' => $this->options['source_y_axis'],
          
    );
	
	$form['destination_x_axis'] = array(
		  '#title' => 'Destination X-Axis',  		
          '#type' => 'select',
          '#options' => $columns,
          '#default_value' => $this->options['destination_x_axis'],
          
    );
	$form['destination_y_axis'] = array(
		  '#title' => 'Destination Y-Axis',  		
          '#type' => 'select',
          '#options' => $columns,
          '#default_value' => $this->options['destination_y_axis'],
          
    );
	
	
	
	
	$form['width'] = array(
      '#type' => 'textfield',
      '#title' => t('Container Width'),
	  '#description' => t('Please use width in pixels like 800px'),
      '#default_value' => $this->options['width'],
      '#parents' => array(
        'style_options',
        'width',
      ),
    );
    
    $form['height'] = array(
      '#type' => 'textfield',
      '#title' => t('Container Height'),
	  '#description' => t('Please use height in pixels like 800px'),
      '#default_value' => $this->options['height'],
      '#parents' => array(
        'style_options',
        'height',
      ),
    );
   
    
    $form['movable_in_browser'] = array(
      '#type' => 'checkbox',
      '#title' => t('movable or unmovable within the browser.'),
      '#default_value' => $this->options['movable_in_browser'],
      
    );
    
  }

}
