<?php

/**
 * @file
 * Contains Views module hooks.
 */

/**
 * Implements hook_views_plugins().
 */
function gojs_views_plugins() {
  $path = drupal_get_path('module', 'gojs');
  $views_path = drupal_get_path('module', 'views');
  return array(
    'module' => 'gojs',
    
    'style' => array(
      'parent' => array(
        'handler' => 'views_plugin_style',
        'path' => "$views_path/plugins",
        'theme file' => 'theme.inc',
        'theme path' => "$views_path/theme",
        'parent' => '',
      ),
      'gojs' => array(
        'title' => t('Gojs'),
        'help' => t('Displays data in an gojs.'),
        'path' => "$path/includes",
        'parent' => 'parent',
        'handler' => 'gojs_views_plugin',
        'theme file' => 'theme.inc',
        'theme path' => "$path/theme",
        'theme' => 'views_view_gojs',
        'uses fields' => TRUE,
        'uses options' => TRUE,
        'uses grouping' => FALSE,
        'uses hook menu' => FALSE,
        'uses hook block' => FALSE,
        'use ajax' => FALSE,
        'use pager' => FALSE,
        'accept attachments' => FALSE,
        'uses row plugin' => TRUE,
        'even empty' => FALSE,
        'type' => 'normal',
      ),
    ),
  );
}